<?php
    get_header();
?>

<div class="jumbotron">
    <h1>Acerca de nosotros</h1>
</div>
<div class="galeria">
    <div class="foto1">
        <img src="<?=get_theme_file_uri("img/road.jpg")?>" alt="" srcset="">
        <img src="<?=get_theme_file_uri("img/food.jpg")?>" alt="" srcset="">
    </div>
</div>
<?php
        while(have_posts()){
            the_post();
            ?>
            <h2><a href="<?=the_permalink()?>"><?=the_title()?></a></h2>
            <p><?=the_content()?></p>
        <?php
        }
    ?>



<?php
    get_footer( );
?>