<?php
//cargamos librerias de terceros
require_once get_template_directory() . '/inc/class-wp-bootstrap-navwalker.php';


function gmateu_config(){

    register_nav_menus(
        array(
            'gmateu_main_menu' => 'gmateu menú principal',
            'gmateu_links' => 'gmateu menú links',
        )
    );


}
add_action( 'after_setup_theme', 'gmateu_config', 0 );

function gmateu_scripts(){
    wp_enqueue_script( "bootstrap_js", get_theme_file_uri("inc/js/bootstrap.min.js"), array("jquery"),"4.5", true );
    wp_enqueue_style(  "bootstrap_css", get_theme_file_uri("inc/css/bootstrap.min.css"),array(), "4.5","all" );
    wp_enqueue_style( "style_css", get_theme_file_uri("style.css"),array(), filemtime(get_template_directory().'/style.css'),"all" );
    wp_enqueue_style( "curtom_css", get_theme_file_uri("css/estils.css"),array(), filemtime(get_template_directory().'/style.css'),"all" );

}
add_action( 'wp_enqueue_scripts', 'gmateu_scripts');