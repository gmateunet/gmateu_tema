<footer>
    <h3>Aquí irá nuestro pie de página</h3>
    <div class="row">
        <div class="col-6">
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Alias, ratione.</p>
        </div>
        <div class="col-3">
        <?php
        wp_nav_menu( array(
                'theme_location'    => 'gmateu_links',
        ));
        ?>
        </div>
        <div class="col-3">
            <p>Lorem ipsum dolor sit amet consectetur.</p>
        </div>
    </div>
</footer>

<?php wp_footer();?>
</body>

</head>